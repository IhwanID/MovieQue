package id.ihwan.movieque

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_detail_movie.*

class DetailMovieActivity : AppCompatActivity() {

    private var title: String = ""
    private var vote_average: String = ""
    private var backdrop_path: String = ""
    private var overview: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_movie)

        val i = intent

        title = i.getStringExtra("title")
        vote_average = i.getStringExtra("vote_average")
        backdrop_path = i.getStringExtra("backdrop_path")
        overview = i.getStringExtra("overview")

        Glide.with(this).load("https://image.tmdb.org/t/p/w500/" + backdrop_path).into(imageMovieDetail)

        titleMovieDetail.text = "Title : " + title
        voteAverageMovieDetail.text = "Rating : " + vote_average
        overviewMovieDetail.text = "Overview : " + overview
    }
}
