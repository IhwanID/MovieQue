package id.ihwan.movieque

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.androidnetworking.AndroidNetworking
import com.androidnetworking.common.Priority
import com.androidnetworking.interfaces.JSONObjectRequestListener
import id.ihwan.movieque.model.Movie
import com.androidnetworking.error.ANError
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.startActivity
import org.json.JSONObject



class MainActivity : AppCompatActivity() {

    val movieArrayList = ArrayList<Movie>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getDataMovie()
        recyclerView.layoutManager =LinearLayoutManager(this)


    }

    fun getDataMovie(){
        movieArrayList.clear()

        AndroidNetworking.get("https://api.themoviedb.org/3/movie/popular?api_key=78adf61cd991fec888c055105c148a44")
            .setPriority(Priority.MEDIUM)
            .build()
            .getAsJSONObject(object : JSONObjectRequestListener {
                override fun onResponse(response: JSONObject) {

                    val results = response.getJSONArray("results")

                    for (i in 0 until results.length()){
                        val resultsObject = results.getJSONObject(i)

                        movieArrayList.add(
                            Movie(
                                resultsObject.getString("title"),
                                resultsObject.getString("vote_average"),
                                resultsObject.getString("backdrop_path"),
                                resultsObject.getString("overview")
                            )
                        )
                    }

                    recyclerView.adapter = RecyclerViewAdapter(this@MainActivity, movieArrayList){
                        startActivity<DetailMovieActivity>(
                            "title" to "${it.title}",
                            "vote_average" to "${it.vote_average}",
                            "backdrop_path" to "${it.backdrop_path}",
                            "overview" to "${it.overview}"
                        )
                    }


                }

                override fun onError(error: ANError) {
                    // handle error
                }
            })

    }
}
