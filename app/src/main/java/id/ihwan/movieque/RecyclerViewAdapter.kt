package id.ihwan.movieque

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import id.ihwan.movieque.model.Movie
import kotlinx.android.synthetic.main.movie_item.view.*

/**
 * Created by ihwan on 08,December,2018
 */

class RecyclerViewAdapter(val context: Context,
                          val items: ArrayList<Movie>,
                          private val listener: (Movie) -> Unit)
    : androidx.recyclerview.widget.RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.movie_item, p0, false))
    }


    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.bindItem(items[p1],listener)
    }

    class ViewHolder(view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bindItem(items: Movie, listener: (Movie) -> Unit){

            itemView.titleMovie.text = items.title

            Glide.with(itemView.context).load("https://image.tmdb.org/t/p/w500/" + items.backdrop_path)
                .into(itemView.imageMovie)

            itemView.imageMovie.setOnClickListener{
                listener(items)
            }
        }
    }


    }
