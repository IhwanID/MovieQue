package id.ihwan.movieque.model

/**
 * Created by ihwan on 08,December,2018
 */
data class Movie(
    val title : String,
    val vote_average : String,
    val backdrop_path : String,
    val overview : String
)